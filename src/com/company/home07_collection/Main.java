package com.company.home07_collection;

//Зробити список з 10 чисел [2,17,13,6,22,31,45,66,100,-18]та:
//        1. Перебрати його циклом while
//        2. перебрати його циклом for
//        3. перебрати циклом while та вивести  числа тільки з непарним індексом
//        4. перебрати циклом for та вивести  числа тільки з непарним індексом
//        5. перебрати циклом while та вивести  числа тільки парні  значення
//        6. перебрати циклом for та вивести  числа тільки парні  значення
//        8. Вивести список в зворотньому порядку.
//        9. Всі попередні завдання (окрім 8), але в зворотньому циклі (с заду на перед)

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> listNumbers = new ArrayList<>();
        listNumbers.add(2);
        listNumbers.add(17);
        listNumbers.add(13);
        listNumbers.add(6);
        listNumbers.add(22);
        listNumbers.add(31);
        listNumbers.add(45);
        listNumbers.add(66);
        listNumbers.add(100);
        listNumbers.add(-18);
        System.out.println(listNumbers);

//        int currentPosition = 0;
//        while (currentPosition < listNumbers.size()) {
//            System.out.println(listNumbers.get(currentPosition));
//            currentPosition++;
//        }

        for (int i = listNumbers.size() - 1; i > 0 ; i--) {

            System.out.println(listNumbers.get(i));
        }
    }
}
