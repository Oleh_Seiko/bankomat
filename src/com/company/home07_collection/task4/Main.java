package com.company.home07_collection.task4;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Person vaysa = new Person(1, "vaysa", 32);
        Person petya = new Person(2, "petya", 31);
        Person kolya = new Person(3, "kolya", 30);
        Person anya = new Person(4, "anya", 29);
        Person masha = new Person(5, "masha", 28);
        Person andriy = new Person(6, "andriy", 30);
        Person maxim = new Person(7, "maxim", 31);
        Person vadym = new Person(8, "vadym", 32);
        Person taras = new Person(9, "taras", 33);

        Auth auth1 = new Auth(5, "login5", "password5");
        Auth auth2 = new Auth(2, "login2", "password2");
        Auth auth3 = new Auth(1, "login1", "password1");
        Auth auth4 = new Auth(7, "login7", "password7");
        Auth auth5 = new Auth(3, "login3", "password3");
        Auth auth6 = new Auth(4, "login4", "password4");
        Auth auth7 = new Auth(8, "login8", "password8");
        Auth auth8 = new Auth(6, "login6", "password6");
        Auth auth9 = new Auth(9, "login9", "password9");

        List<Person> personList = new ArrayList<>();
        personList.add(vaysa);
        personList.add(petya);
        personList.add(kolya);
        personList.add(anya);
        personList.add(masha);
        personList.add(andriy);
        personList.add(maxim);
        personList.add(vadym);
        personList.add(taras);

        System.out.println("Person " + personList);

        List<Auth> authList = new ArrayList<>();
        authList.add(auth1);
        authList.add(auth2);
        authList.add(auth3);
        authList.add(auth4);
        authList.add(auth5);
        authList.add(auth6);
        authList.add(auth7);
        authList.add(auth8);
        authList.add(auth9);

        System.out.println("Auth " + authList);
        List<Person_Auth> personAuthList = merge(personList, authList);
        System.out.println("PersonAuthList " + personAuthList);

    }


    private static List<Person_Auth> merge(List<Person> personList, List<Auth> authList) {

        List<Person_Auth> personAuthList = new ArrayList<>();
        for (int i = 0; i < personList.size(); i++) {
            for (int j = 0; j < authList.size(); j++) {

                if(personList.get(i).getId() == authList.get(j).getId()) {
                    Person_Auth person_auth = new Person_Auth();
                    person_auth.setId(personList.get(i).getId());
                    person_auth.setName(personList.get(i).getName());
                    person_auth.setAge(personList.get(i).getAge());
                    person_auth.setLogin(authList.get(j).getLogin());
                    person_auth.setPassword(authList.get(j).getPassword());
                    System.out.println("SOUT person_aut " + person_auth);
                    personAuthList.add(person_auth);
                }

            }

        }

        return personAuthList;
    }
}
