package com.company.home07_collection.task4;

import java.util.Objects;

public class Person_Auth {

    private int id;
    private String name;
    private int age;
    private String login;
    private String password;

    public Person_Auth() {
    }

    public Person_Auth(int id, String name, int age, String login, String password) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.login = login;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person_Auth twice = (Person_Auth) o;
        return id == twice.id &&
                age == twice.age &&
                name.equals(twice.name) &&
                login.equals(twice.login) &&
                password.equals(twice.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, age, login, password);
    }

    @Override
    public String toString() {
        return "Person_Auth{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
