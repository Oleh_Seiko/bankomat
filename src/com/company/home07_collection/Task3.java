package com.company.home07_collection;

//1. используя Math.random заполнить список 100 элементами.
//        диапазон рандома от -999 до 999.
//        2. Вывести на консоль  каждый третий елемент
//        3. Вывести на консоль  каждый третий елемент
//        но при условии что он имеет парное значение.
//        4. Вывести на консоль  каждый третий елемент
//        но при условии что он имеет парное значение и
//        записать их в другой список.

import java.util.ArrayList;
import java.util.List;

public class Task3 {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        System.out.println(list.size());
        List<Integer> listNew = new ArrayList<>();
//
        for (int i = 0; i < 100; i+=3) {
            int element =(int) ((Math.random()*(999 - (- 999))) +(-999));
            list.add(element);
            if (element%2 == 0){
                System.out.println("EleventParnuj " + element);
                listNew.add(element);
            }

        }
        System.out.println("Size " + list.size());
        System.out.println(list);
        System.out.println("Third " + list);
        System.out.println("NEW List " + listNew);
    }
}
