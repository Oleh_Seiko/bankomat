package com.company.home07_collection;

//Створити пустий список та :
//        1. Заповнити його 50 парними числами.
//        2. Заповнити його 50 непарними числами.

import java.util.ArrayList;
import java.util.List;

public class Task2 {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < 100; i++) {
            if (i % 2 == 0) {
                list.add(i);
            }
        }
        System.out.println(list);
    }
}
