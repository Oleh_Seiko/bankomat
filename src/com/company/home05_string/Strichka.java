package com.company.home05_string;

//написати метод, який вирізає зі стрічки вказаний діапазон і вертає його

public class Strichka {

    public String st = "Я вивчаю програмування, але мені це тяжко наразі дається.";


    public String cutSentence(String string, int indexStart, int indexEnd) {
        String substring = string.substring(indexStart, indexEnd);
        return substring;
    }

    public String[] splitSentence(String str, String symbol) {
        String[] split = str.split(symbol);
        return split;
    }
}
