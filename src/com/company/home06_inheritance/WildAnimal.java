package com.company.home06_inheritance;

import java.util.Objects;

public class WildAnimal extends Animal {

    public String livesInForest;

    public WildAnimal() {

    }

    public WildAnimal(String eyes, String livesInForest) {
        super(eyes);
        this.livesInForest = livesInForest;
    }

    public String getLivesInForest() {
        return livesInForest;
    }

    public void setLivesInForest(String livesInForest) {
        this.livesInForest = livesInForest;
    }

    @Override
    public void voice() {
        System.out.println("WildAnimal");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        WildAnimal that = (WildAnimal) o;
        return Objects.equals(livesInForest, that.livesInForest);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), livesInForest);
    }

    @Override
    public String toString() {
        return "WildAnimal{" +
                "livesInForest='" + livesInForest + '\'' +
                '}';
    }
}
