package com.company.home06_inheritance;

import java.util.Objects;

public class Wolf extends WildAnimal {

    public String haveBigTeeth;

    public Wolf() {

    }

    public Wolf(String eyes, String livesInForest, String haveBigTeeth) {
        super(eyes, livesInForest);
        this.haveBigTeeth = haveBigTeeth;
    }

    public String getHaveBigTeeth() {
        return haveBigTeeth;
    }

    public void setHaveBigTeeth(String haveBigTeeth) {
        this.haveBigTeeth = haveBigTeeth;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Wolf wolf = (Wolf) o;
        return Objects.equals(haveBigTeeth, wolf.haveBigTeeth);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), haveBigTeeth);
    }

    @Override
    public void voice() {
        System.out.println("AYYYYYYY");
    }

    @Override
    public String toString() {
        return "Wolf{" +
                "haveBigTeeth='" + haveBigTeeth + '\'' +
                '}';
    }
}
