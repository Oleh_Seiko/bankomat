package com.company.home06_inheritance;

import java.util.Objects;

public class Animal {

    public String eyes;

    public Animal() {
    }

    public Animal(String eyes) {
        this.eyes = eyes;
    }

    public String getEyes() {
        return eyes;
    }

    public void setEyes(String eyes) {
        this.eyes = eyes;
    }

    public void voice() {
        System.out.println("Animallllllll");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return Objects.equals(eyes, animal.eyes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(eyes);
    }

    @Override
    public String toString() {
        return "Animal{" +
                "eyes='" + eyes + '\'' +
                '}';
    }
}
