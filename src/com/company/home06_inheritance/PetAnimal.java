package com.company.home06_inheritance;

import java.util.Objects;

public class PetAnimal extends Animal {

    public String theOwner;

    public PetAnimal() {
    }

    public PetAnimal(String eyes, String theOwner) {
        super(eyes);
        this.theOwner = theOwner;
    }

    @Override
    public void voice() {
        System.out.println("PET Animal");
    }

    public String getTheOwner() {
        return theOwner;
    }

    public void setTheOwner(String theOwner) {
        this.theOwner = theOwner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PetAnimal pet = (PetAnimal) o;
        return Objects.equals(theOwner, pet.theOwner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), theOwner);
    }

    @Override
    public String toString() {
        return "PetAnimal{" +
                "theOwner='" + theOwner + '\'' +
                '}';
    }
}
