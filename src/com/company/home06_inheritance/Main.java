package com.company.home06_inheritance;

public class Main {
    public static void main(String[] args) {
        Animal animal = new Animal();
        animal.voice();

        Cat cat = new Cat();
        cat.voice();

        Wolf wolf =new Wolf();
        wolf.voice();

        PetAnimal pet = new PetAnimal();
        pet.voice();

        Animal animal1 = new Cat();
        animal1.voice();

        Fox fox = new Fox();
        fox.voice();

        WildAnimal wildAnimal = new WildAnimal();
        wildAnimal.voice();


    }
}
