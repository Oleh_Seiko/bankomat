package com.company.home06_inheritance;

import java.util.Objects;

public class Dog extends PetAnimal {
    public String trustInPeople;

    public Dog(String eyes, String theOwner, String trustInPeople) {
        super(eyes, theOwner);
        this.trustInPeople = trustInPeople;
    }

    @Override
    public void voice() {
        System.out.println("Gaf-gaf");
    }

    public String getTrustInPeople() {
        return trustInPeople;
    }

    public void setTrustInPeople(String trustInPeople) {
        this.trustInPeople = trustInPeople;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Dog dog = (Dog) o;
        return Objects.equals(trustInPeople, dog.trustInPeople);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), trustInPeople);
    }

    @Override
    public String toString() {
        return "Dog{" +
                "trustInPeople='" + trustInPeople + '\'' +
                '}';
    }
}
