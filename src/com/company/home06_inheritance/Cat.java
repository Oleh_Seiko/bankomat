package com.company.home06_inheritance;

import java.util.Objects;

public class Cat extends PetAnimal {

    public int claws;

    public Cat() {
    }
    public Cat(String eyes, String theOwner, int claws) {
        super(eyes, theOwner);
        this.claws = claws;
    }



    @Override
    public void voice() {
        System.out.println("MAY-MAY");
    }

    public int getClaws() {
        return claws;
    }

    public void setClaws(int claws) {
        this.claws = claws;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Cat cat = (Cat) o;
        return claws == cat.claws;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), claws);
    }

    @Override
    public String toString() {
        return "Cat{" +
                "claws=" + claws +
                '}';
    }
}
