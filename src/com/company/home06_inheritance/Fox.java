package com.company.home06_inheritance;

import java.util.Objects;

public class Fox extends WildAnimal {

    public String redColor;

    public Fox() {
    }

    public Fox(String eyes, String livesInForest, String redColor) {
        super(eyes, livesInForest);
        this.redColor = redColor;
    }

    @Override
    public void voice() {
        System.out.println("FOX");
    }

    public String getRedColor() {
        return redColor;
    }

    public void setRedColor(String redColor) {
        this.redColor = redColor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Fox fox = (Fox) o;
        return Objects.equals(redColor, fox.redColor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), redColor);
    }

    @Override
    public String toString() {
        return "Fox{" +
                "redColor='" + redColor + '\'' +
                '}';
    }
}
