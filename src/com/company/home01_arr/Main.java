package com.company.home01_arr;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        User user1 = new User("Petya", 89.3, 166);
        User user2 = new User("Vasya", 90.1, 190);
        User user3= new User("Kolya", 80.1, 145);
        User user4 = new User("Galya", 62.0, 173);

        double weightAll = user1.weight + user2.weight + user3.weight + user4.weight;
        System.out.println("weightAll = " + weightAll);

        double haightAll = user1.height + user2.height + user3.height + user4.height;
        System.out.println("haightAll = " + haightAll);

        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

        User[] arrUsers = new User[4];
        arrUsers[0] = user1;
        arrUsers[1] = user2;
        arrUsers[2] = user3;
        arrUsers[3] = user4;

        System.out.println(Arrays.toString(arrUsers));

        double sumHaight = 0;
        double sumWaight = 0;

        for(int i = 0; i < arrUsers.length; i++){
           double haight = arrUsers[i].height;
           sumHaight = sumHaight + haight;

            double weight = arrUsers[i].weight;
            sumWaight = sumWaight + weight;


            System.out.println("haight = " + haight);
            System.out.println("weight = " + weight);
        }
        System.out.println("sumHaight = " + sumHaight);
        System.out.println("sumWeight = " + sumWaight);
    }
}
