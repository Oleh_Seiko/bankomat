package com.company.home03_for;

import java.util.Scanner;

public class Bankomat {
    private static String login = "log";
    private static String password = "pas";
    private static int account = 1000;

    public static void main(String[] args) {
        boolean work = true;
        while (work) {
            String enteredLogin = null;
            String enteredPassword = null;
            String later = null;

            Scanner scanner = new Scanner(System.in);
            while (!login.equals(enteredLogin)) {
                System.out.println("Уведіть логін");
                enteredLogin = scanner.nextLine();
            }

            Scanner sc = new Scanner(System.in);
            while (!password.equals(enteredPassword)) {
                System.out.println("Уведіть пароль");
                enteredPassword = sc.nextLine();

                System.out.println("Меню: ");
                System.out.println("a) Подивитися стан рахунку");
                System.out.println("b) Зняти кошти");
                System.out.println("c) Покласти кошти");
                System.out.println("d) Вихід");
                System.out.println("e) Продовжити роботу");

                Scanner scLater = new Scanner(System.in);
                later = scLater.nextLine();
                while (!later.equals("d")) {
                    if (later.equals("a")) {
                        System.out.println("Залишок на вашому рахунку " + account + "грн.");
                    }

                    if (later.equals("b")) {
                        System.out.println("Яку суму зняти?");
                        Scanner scMin = new Scanner(System.in);
                        int sumMin = scMin.nextInt();
                        int sumMinus = account - sumMin;
                        if (sumMinus >= 0) {
                            account = sumMinus;
                            System.out.println("Ви зняли " + sumMin + " У вас залишилось " + sumMinus + " грн.");
                        } else {
                            System.out.println("Введена вами сума перевищує залишок коштів на карті.");
                        }
                    }

                    if (later.equals("c")) {
                        System.out.println("На яку суму поповняємо?");
                        Scanner scPlus = new Scanner(System.in);
                        int sumPlus = scPlus.nextInt();
                        account = account + sumPlus;
                        System.out.println("Ваш рахунок поповнено на " + sumPlus + " грн.");
                    } else {
                        System.out.println("Виберіть іншу операцію:");
                        System.out.println("a) Подивитися стан рахунку");
                        System.out.println("b) Зняти кошти");
                        System.out.println("c) Покласти кошти");
                        System.out.println("d) Вихід");
                        System.out.println("e) Продовжити роботу");
                    }

                    Scanner scMenu1 = new Scanner(System.in);
                    later = scMenu1.nextLine();
                }
            }

            System.out.println("next step");
            work = false;
        }


    }
}
