package com.company.home06_interfase.interfases02;

import java.util.Arrays;

//Створити інтерфейс метод  creator() якого вертає числовий масив.
//        Реалізувати його в двох класах.
//        В першому класі метод повинен формувати масив рандомних чисел в діапазоні 0 до 200
//        В другому класі метод повинен формувати масив рандомних чисел в діапазоні  0 до х?
//        В першому класі та другому класі є поле lenght яке визначає довжину масиву який ви булите генерувати
//        В другому класі є додаткове поле  х , яке визначає діапазон.
//        В першому классі діапазон завжди постійний. Число ,яке характеризує діапазон оголошено як змінна інтерфейсу

public class Main {
    public static void main(String[] args) {
//        Randoming random1 = new Randoming(10);
//        int[] ran1 = random1.creator();
//        System.out.println(Arrays.toString(ran1));

        RandomMax randomMax = new RandomMax(10, 5);
        int[] ranMax = randomMax.creator();
        System.out.println(Arrays.toString(ranMax));
    }
}
