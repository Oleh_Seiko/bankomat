package com.company.home06_interfase.interfases04;

public class MixOne implements Singer, Runner{



    @Override
    public void run() {
        System.out.println("MixOne: I running this distance ");
    }

    @Override
    public void sing() {
        System.out.println("MixOne I sing");
    }
}
