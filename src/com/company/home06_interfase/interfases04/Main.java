package com.company.home06_interfase.interfases04;

public class Main {
    public static void main(String[] args) {

        Singer singer1 = new MixOne();
        Singer singer2 = new MixOne();
        Singer singer3 = new MixTwo();
        Singer singer4 = new MixTwo();
        Singer singer5 = new MixTwo();


        Singer[] singers = {singer1, singer2, singer3, singer4, singer5};
        for (Singer singer : singers) {
            singer.sing();
        }


    }
}
