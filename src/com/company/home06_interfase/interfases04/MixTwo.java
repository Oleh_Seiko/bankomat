package com.company.home06_interfase.interfases04;

public class MixTwo implements Singer, Runner {

    @Override
    public void run() {
        System.out.println("MixTwo: I running this distance ");
    }

    @Override
    public void sing() {
        System.out.println("MixTwo I sing");
    }
}
