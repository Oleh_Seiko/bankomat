package com.company.home06_interfase;

public interface ArraySaver {
    int[] save(int a);

    int findOne(int a);

    int[] findAll();

    void setNumbers(int[] numbers);
    void setNums(int[] numbers);

    int[] setFindOne(int[] numbers);

}
