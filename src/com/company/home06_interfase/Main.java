package com.company.home06_interfase;

import java.util.Arrays;

public class Main {

    private static int[] number = {1, 2, 4, 8, -2, 10, 25, 3, 12};

    public static void main(String[] args) {
//        ArraySaver arraySaver1 = new FirstClass();
//        save(number, -2, arraySaver1);
//        System.out.println(Arrays.toString(number));
//        System.out.println("~~~~~~~~~~~~~~~~~");

//        int one = findOne(number, 25, arraySaver1);
//        System.out.println("One " + one);
//        int[] all = findAll(number, arraySaver1);
//        System.out.println(Arrays.toString(all));

        ArraySaver arraySaver2 = new SecondClass();
        save(number, -2, arraySaver2);
        System.out.println(Arrays.toString(number));
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~");

        int two = findOne(number, 25, arraySaver2);
        System.out.println("Two " + " " +two);

        int[] evenNums = findAll(number, arraySaver2);
        System.out.println(Arrays.toString(evenNums));

//
    }

    private static void save(int[] arr, int obj, ArraySaver arraySaver) {
        arraySaver.setNumbers(arr);
        arraySaver.setNums(arr);
        int[] saved = arraySaver.save(obj);
        number = saved;
    }

    private static int findOne(int[] arr, int obj, ArraySaver arraySaver) {
        arraySaver.setFindOne(arr);
        int findOneElem = arraySaver.findOne(obj);
        return findOneElem;
    }

    private static int[] findAll(int[] arr, ArraySaver arraySaver) {
        arraySaver.setNums(arr);
        return arraySaver.findAll();
    }
}
