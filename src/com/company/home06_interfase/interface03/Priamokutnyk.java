package com.company.home06_interfase.interface03;

public class Priamokutnyk implements Squ{
    double fialdOne;
    double fialdTwo;

    public Priamokutnyk() {
    }

    public Priamokutnyk(int fialdOne, int fialdTwo) {
        this.fialdOne = fialdOne;
        this.fialdTwo = fialdTwo;
    }

    @Override
    public double square() {
        return (fialdOne*fialdTwo);
    }
}
