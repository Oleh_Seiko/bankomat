package com.company.home06_interfase.interface03;

public class Kvadrat implements Squ {
    double fiald;

    public Kvadrat() {
    }

    public Kvadrat(double fiald) {
        this.fiald = fiald;
    }

    public double getFiald() {
        return fiald;
    }

    public void setFiald(double fiald) {
        this.fiald = fiald;
    }

    @Override
    public String toString() {
        return "Kvadrat{" +
                "fiald=" + fiald +
                '}';
    }

    @Override
    public double square() {
        double pl = fiald * fiald;
        System.out.println(pl);
        return pl;
    }
}
