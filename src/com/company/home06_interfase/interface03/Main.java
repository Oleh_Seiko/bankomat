package com.company.home06_interfase.interface03;

//Створити інтрефейс, з методом square() який повертає число з поаваючою крапкою.
//        Суть метода square() - обчисленя полщі в залежності від фігури.
//        Створити двіі реалізації цього інтерфейсу.
//        Одна це Kvadrat. цей класс має поля, які харатеризують його сторони.
//        Друга - Priamokutnyk цей класс має поля, які харатеризують його сторони.

public class Main {
    public static void main(String[] args) {
        Squ sq = new Kvadrat(5);
        double squareKvadrat = sq.square();
        System.out.println("Sq Kvadrat = " + squareKvadrat);

        Squ sq2 = new Priamokutnyk(5,2);
        double squarePriamokutnyk = sq2.square();
        System.out.println("Sq Priamokutnyk = " + squarePriamokutnyk);
    }
}
