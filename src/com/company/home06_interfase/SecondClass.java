package com.company.home06_interfase;

import java.util.Arrays;

public class SecondClass implements ArraySaver {
    private int[] nums;

    public void setNums(int[] nums) {
        this.nums = nums;
    }

    @Override
    public int[] setFindOne(int[] numbers) {
        return new int[0];
    }

    @Override
    public int[] save(int a) {
        int[] arrLength = Arrays.copyOf(nums, nums.length + 1);
        arrLength[arrLength.length - 1] = a;
        nums = arrLength;
        return nums;
    }

    @Override
    public int findOne(int a) {
        for (int i = 0; i < nums.length; i++) {
            if (a == nums[i]) {
                return a;
            }

        }
        return -5555;
    }

    @Override
    public int[] findAll() {
        int counter = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] % 2 == 0) {
                counter++;
            }
        }
        System.out.println("Counter " + " " +counter);

        int[] evenNums = new int[counter];

        int position = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] % 2 == 0) {
                evenNums[position] = nums[i];
                position++;
            }
        }

        System.out.println("Pairs " + Arrays.toString(evenNums));
        return evenNums;
    }

    @Override
    public void setNumbers(int[] numbers) {

    }
}
